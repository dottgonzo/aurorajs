var exec=require('promised-exec'),
PouchDB=require('pouchdb'),
verb=require('verbo'),
Promise = require('promise');


function prepare_address(addresses){
  var readdr=[];
  for(var i=0;i<addresses.length;i++){
    readdr[i]={uuid:addresses[i].uuid,dev:addresses[i].dev,address:addresses[i].address}

  }
  return JSON.stringify(readdr)
}

function Aurorajs(addresses,timezone,db){
  this.addresses=addresses;
  this.timezone=timezone;
  this.db=db
}
Aurorajs.prototype.data=function(){
  var addresses=prepare_address(this.addresses);
  var timezone=this.timezone;

  return new Promise(function (resolve, reject) {
    exec(__dirname+'/aurora.sh -a \''+addresses+'\' -t "'+timezone+'"').then(function(data){
      resolve(JSON.parse(data));
    }).catch(function(err){
      reject(err)
    });
  });
};
Aurorajs.prototype.save=function(){
  var addresses=prepare_address(this.addresses);
  var timezone=this.timezone;

  var db=new PouchDB(this.db);
  return new Promise(function (resolve, reject) {
    exec(__dirname+'/aurora.sh -a \''+addresses+'\' -t "'+timezone+'"').then(function(data){
      var docs=JSON.parse(data);
      var topushs=[];
      for(var i=0;i<docs.length;i++){


        if (docs[i]._id){
          topushs.push(docs[i])
        }
      }
      if(topushs.length>0){


        db.bulkDocs(topushs).then(function(){
          resolve(docs);

        }).catch(function(){

          reject({error:"pouch"})

        })
      } else{
        resolve(docs);

      }
    }).catch(function(){
      reject({error:"exec"})
    })
  })
};
module.exports= Aurorajs
